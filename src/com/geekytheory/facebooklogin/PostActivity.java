package com.geekytheory.facebooklogin;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;



public class PostActivity extends Activity implements OnClickListener {
	
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions", "email");
	@SuppressWarnings("unused")
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	@SuppressWarnings("unused")
	private boolean pendingPublishReauthorization = false;
	
	private String TAG = "PostActivity";

	private TextView lblName;
	String permissions[] = {
		    "user_about_me",
		    "user_activities",
		    "user_birthday",
		    "user_checkins",
		    "user_education_history",
		    "user_events",
		    "user_groups",
		    "user_hometown",
		    "user_interests",
		    "user_likes",
		    "user_location",
		    "user_notes",
		    "user_online_presence",
		    "user_photo_video_tags",
		    "user_photos",
		    "user_relationships",
		    "user_relationship_details",
		    "user_religion_politics",
		    "user_status",
		    "user_videos",
		    "user_website",
		    "user_work_history",
		    "email",
		    "read_friendlists",
		    "read_insights",
		    "read_mailbox",
		    "read_requests",
		    "read_stream",
		    "xmpp_login",
		    "ads_management",
		    "create_event",
		    "manage_friendlists",
		    "manage_notifications",
		    "offline_access",
		    "publish_checkins",
		    "publish_stream",
		    "rsvp_event",
		    "sms",
		    //"publish_actions",
		 
		    "manage_pages"
		 
		  };
	
	EditText title, caption, description, link;
	Button publish;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post);
		lblName = (TextView) findViewById(R.id.lblName);
		lblName.setText("Hi, "+getIntent().getExtras().getString("name")+"!");
		
		title = (EditText) findViewById(R.id.etTitle);
		caption = (EditText) findViewById(R.id.etCaption);
		description = (EditText) findViewById(R.id.etDescription);
		link = (EditText) findViewById(R.id.etLink);
		
		publish = (Button) findViewById(R.id.bPost);
		publish.setOnClickListener(this);
		
		
	}
	
	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
	    for (String string : subset) {
	        if (!superset.contains(string)) {
	            return false;
	        }
	    }
	    return true;
	}
	
	@Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if(v.getId()==publish.getId()){
            //al pulsar sobre el boton publicar invocamos el método publicaMuro()
        	publishStory(); 
        }       
    }

	private void publishStory() {
	    Session session = Session.getActiveSession();

	    if (session != null){

	        // Check for publish permissions    
	        //List<String> permissions = session.getPermissions();
	    	List<String> permissions = Arrays.asList("publish_stream", "publish_actions", "email", "basic_info");
	        if (!isSubsetOf(PERMISSIONS, permissions)) {
	            pendingPublishReauthorization = true;
	            Session.NewPermissionsRequest newPermissionsRequest = new Session
	                    .NewPermissionsRequest(this, PERMISSIONS);
	        session.requestNewPublishPermissions(newPermissionsRequest);
	            return;
	        }

	        Bundle postParams = new Bundle();
	        postParams.putString("name", title.getText().toString());
	        postParams.putString("caption", caption.getText().toString());
	        postParams.putString("description", description.getText().toString());
	        postParams.putString("link", link.getText().toString());
	        //postParams.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");

	        Request.Callback callback= new Request.Callback() {
	            public void onCompleted(Response response) {
	                JSONObject graphResponse = response
	                                           .getGraphObject()
	                                           .getInnerJSONObject();
	                String postId = null;
	                try {
	                    postId = graphResponse.getString("id");
	                } catch (JSONException e) {
	                    Log.i(TAG,
	                        "JSON error "+ e.getMessage());
	                }
	                FacebookRequestError error = response.getError();
	                if (error != null) {
	                    Toast.makeText(getApplicationContext(),
	                         error.getErrorMessage(),
	                         Toast.LENGTH_SHORT).show();
	                    } else {
	                        Toast.makeText(getApplicationContext(), 
	                             "Successfully posted!",
	                             Toast.LENGTH_LONG).show();
	                }
	            }
	        };

	        Request request = new Request(session, "me/feed", postParams, 
	                              HttpMethod.POST, callback);

	        RequestAsyncTask task = new RequestAsyncTask(request);
	        task.execute();
	    }

	}
	
	
}
