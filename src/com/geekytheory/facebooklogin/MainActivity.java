package com.geekytheory.facebooklogin;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.Builder;
import com.facebook.Session.OpenRequest;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

public class MainActivity extends Activity {

	private String TAG = "MainActivity";
	LoginButton authButton;
	Session session;
	Activity activity = this;
	UiLifecycleHelper uiHelper;
	Session.StatusCallback callback;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		callback = new Session.StatusCallback() {
	        @Override
	        public void call(Session session, SessionState state, Exception exception) {
	            Log.i(TAG, "calling session callback");
	            onSessionStateChange(session, state, exception);
	        }
	    };
	    session = Session.getActiveSession();
	    if(session==null){
	        Log.i(TAG, "session is null");
	        //Session.openActiveSession(this, false, callback);
	        if(Session.openActiveSession(this, false, callback) == null){
	            notLoggedIn();
	        }
	    }
	    else{
	        if(session.isClosed()){
	            Log.i(TAG, "session is closed");	            
	            notLoggedIn();
	        }
	        else if(session.isOpened()){
	            Log.i(TAG, "session is opened");
	            Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user,
							Response response) {
						if (user != null) {
							loggedIn(user.getName());
						} else {
							notLoggedIn();
						}
					}
				});
		//loggedIn("mario");
	        }
	    }
	    uiHelper = new UiLifecycleHelper(this, callback);
	    uiHelper.onCreate(savedInstanceState);
	}

	
	protected void onSessionStateChange(Session session, SessionState state, Exception exception) {
		//this.onSessionStateChange(session, state, exception);
		if (state.isOpened()) {
			// Session open
			Log.i(TAG, "Session opened");
			Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
						@Override
						public void onCompleted(GraphUser user,
								Response response) {
							if (user != null) {
								loggedIn(user.getName());
							} else {
								notLoggedIn();
							}
						}
					});
			//loggedIn("mario");

		} else if (state.isClosed()) {
			// Session closed
			Log.i(TAG, "Session closed");
			notLoggedIn();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);
	}

	public void loggedIn(String user_name) {
		Intent i = new Intent(this, PostActivity.class);
		i.putExtra("name", user_name);
		startActivity(i);
	}

	public void notLoggedIn() {
		Intent i = new Intent(this, Login_Activity.class);
		startActivity(i);
	}
}